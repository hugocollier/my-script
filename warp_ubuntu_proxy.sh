#!/bin/bash

# Add cloudflare gpg key
curl -fsSL https://pkg.cloudflareclient.com/pubkey.gpg | sudo gpg --yes --dearmor --output /usr/share/keyrings/cloudflare-warp-archive-keyring.gpg


# Add this repo to your apt repositories
echo "deb [arch=amd64 signed-by=/usr/share/keyrings/cloudflare-warp-archive-keyring.gpg] https://pkg.cloudflareclient.com/ $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/cloudflare-client.list

# Install
sudo apt-get update && sudo apt-get install cloudflare-warp -y

# register
warp-cli register

# Set WARP to proxy mode
warp-cli set-mode proxy

# blocking malware & adult content
# warp-cli set-families-mode full

# Set auto-start (in fact, the next command will automatically set enable-always-on, this command does not need to be executed)
warp-cli enable-always-on

# Connect to the CloudFlare network
warp-cli connect


# test connection:$ netstat -naotp | grep LIST
# 测试WARP Socks5连接,回显结果中显示warp=on证明WARP配置正确
#$ curl https://www.cloudflare.com/cdn-cgi/trace/ --proxy socks5://127.0.0.1:40000 | grep warp

# 也可以通过WARP Socks5连接ifconfig.me,查看WARP为VPS分配的公网IP
#$ curl ifconfig.me --proxy socks5://127.0.0.1:40000