
# Install hestiacp modified to install clamd & spamd
```
wget https://gitlab.com/hugocollier/my-script/-/raw/main/hestiacp/hst-install.sh
```
make sure to change these default values or get from here https://hestiacp.com/install.html
```
bash hst-install.sh --hostname cpanel.temp.eu.org --email temp@gmail.com --multiphp yes --proftpd yes --interactive no --force
```



This is the best free setup we’ve found for running a webserver plus mail box on Oracles free tier. Once setup, its super easy to use, everything is done through the web panel.

<iframe title="Best Free VPS Setup 2022 (Hestia on Oracle Cloud Free Tier) Complete Website + Email Tutorial" width="720" height="405" src="https://www.youtube.com/embed/Hz58Zkke4VE?feature=oembed" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>

[Direct Link to video tutorial](https://youtu.be/Hz58Zkke4VE)



The advantages that make this my recommended Oracle cloud free tier setup are:

-   Runs perfectly on ARM, allowing us to access up to 4 CPU and 24GB ram FREE
-   Simple and friendly interface
-   Easy app install, 1-click WordPress
-   NGINX Caching for fast webserver performance
-   Easy mailbox setup, easy to add SMTP relay to provide improved deliverability
-   Default security setup is very solid
-   Auto updates by default

The three commands we used to install the panel are below here.

You can copy them here, and if you’re using Putty, just right click to paste them into the terminal.  
You’ll need to change some parts to match your own details for email (admin@example.com), password (exampledemopassword) and hostname (hcp.example.com). I’d recommend using a subdomain like ‘hcp’ for your panel as I’ve done here.

```
** 1. Change to sudo **

sudo su -

** 2. Get the install script **

wget https://raw.githubusercontent.com/hestiacp/hestiacp/release/install/hst-install.sh


** 3. Important - CHANGE THE NEXT LINE TO USE YOUR OWN DETAILS, recommend using a subdomain like 'hcp' for your panel as I've done here **

bash hst-install.sh --interactive no --email admin@example.com --password exampledemopassword --hostname hcp.example.com -f --port 2083 --multiphp yes

** After its done, reboot with: **

reboot

```

For a smoother setup you can point the subdomain for your hostname over to the Oracle public IP address. In our video we used Namecheap, but all domain hosts will have a similar setup.

After installing, the ports you’ll need to add to your ingress rules are here:

```
8083,80,443,143,993,110,995,25,465,587
```

## Tips for Using Hestia With Cloudflare

Cloudflare doesn’t allow port 8083, so you can change Hestia to use 2083 instead in your terminal:

```
sudo su -

v-change-sys-port 2083
```

Make sure you have all the ports allowed in the Oracle ingress rules:

```
2083,80,443,143,993,110,995,25,465,587
```

It’s good to use Cloudflare origin SSL for maximum performance, you also get 15 year certificates and don’t need to worry about updating certificates for while 🙂

To do this, you can add the Cloudflare certificate authority to your server like this:

```
sudo su -

wget https://developers.cloudflare.com/ssl/static/origin_ca_rsa_root.pem

mv origin_ca_rsa_root.pem origin_ca_rsa_root.crt

cp origin_ca_rsa_root.crt /usr/local/share/ca-certificates

update-ca-certificates
```

### Cloudflare Email Tips:

For the A records for mail and webmail DNS, change them to DNS only:

![](https://gitlab.com/hugocollier/my-script/-/raw/main/images/hestia/dnswebmail01.png)

Then use ‘lets encrypt’ for SSL on your mail domain rather than Cloudflare:

![](https://gitlab.com/hugocollier/my-script/-/raw/main/images/hestia/dnswebmail02.png)