# my-script
some of my scripts that are extracted from the internet 

## bbr1 from teddysun (extracted from https://github.com/FranzKafkaYu/x-ui used to be good)

```
bash <(curl -L -s https://gitlab.com/hugocollier/my-script/-/raw/main/bbr1.sh)
```

## bbr2 from https://github.com/mhsanaei/3x-ui

```
bash <(curl -L -s https://gitlab.com/hugocollier/my-script/-/raw/main/bbr2.sh)
```

## Opening CF ports in ip-tables
OCI default firewall is ip-tables
```
bash <(curl -L -s https://gitlab.com/hugocollier/my-script/-/raw/main/open_cf_ports.sh)
```

## acme ssl from https://github.com/FranzKafkaYu/x-ui/blob/main/x-ui_en.sh

Support one level wildcard domain ssl certificate
```
bash <(curl -L -s https://gitlab.com/hugocollier/my-script/-/raw/main/acme_ssl.sh)
```

## Certbot certificate
No wildcard domain ssl certificate supported

```
sudo apt install snapd
snap install core; snap refresh core
snap install --classic certbot
ln -s /snap/bin/certbot /usr/bin/certbot

certbot certonly --standalone --register-unsafely-without-email --non-interactive --agree-tos -d <Your Domain Name>
```

```
apt-get install certbot -y
certbot certonly --standalone --agree-tos --register-unsafely-without-email -d yourdomain.com
certbot renew --dry-run
```

## Warp proxy

```
bash <(curl -L -s https://gitlab.com/hugocollier/my-script/-/raw/main/warp_ubuntu_proxy.sh)
```
- blocking malware & adult content
`warp-cli set-families-mode full`

Xray config with warp added : [config](https://gitlab.com/hugocollier/my-script/-/raw/main/xray-warp-all-conf.json)

### Warp interface

```
wget -N https://gitlab.com/fscarmen/warp/-/raw/main/menu.sh && bash menu.sh d
```


## Open fastpanel port

```
bash <(curl -L -s https://gitlab.com/hugocollier/my-script/-/raw/main/fastpanel-ports.sh)
```