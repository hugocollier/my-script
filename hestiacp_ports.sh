#!/bin/bash

red='\033[0;31m'
green='\033[0;32m'
yellow='\033[0;33m'
plain='\033[0m'

#Add some basic function here
function LOGD() {
    echo -e "${yellow}[DEG] $* ${plain}"
}

function LOGE() {
    echo -e "${red}[ERR] $* ${plain}"
}

function LOGI() {
    echo -e "${green}[INF] $* ${plain}"
}



sudo iptables -I INPUT -s 0.0.0.0/0 -p tcp --dport 8083 -j ACCEPT
sudo iptables -I INPUT -s 0.0.0.0/0 -p tcp --dport 80 -j ACCEPT
sudo iptables -I INPUT -s 0.0.0.0/0 -p tcp --dport 443 -j ACCEPT
sudo iptables -I INPUT -s 0.0.0.0/0 -p tcp --dport 143 -j ACCEPT
sudo iptables -I INPUT -s 0.0.0.0/0 -p tcp --dport 993 -j ACCEPT
sudo iptables -I INPUT -s 0.0.0.0/0 -p tcp --dport 110 -j ACCEPT
sudo iptables -I INPUT -s 0.0.0.0/0 -p tcp --dport 995 -j ACCEPT
sudo iptables -I INPUT -s 0.0.0.0/0 -p tcp --dport 25 -j ACCEPT
sudo iptables -I INPUT -s 0.0.0.0/0 -p tcp --dport 465 -j ACCEPT
sudo iptables -I INPUT -s 0.0.0.0/0 -p tcp --dport 587 -j ACCEPT
sudo iptables -I INPUT -s 0.0.0.0/0 -p tcp --dport 53 -j ACCEPT
sudo netfilter-persistent save

LOGI "Opening Hestiacp required ports successful"