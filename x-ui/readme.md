# X-UI GOOD MY Last

- Used Centos 8

## Updating System

```bash
sudo su -
yum update -y
```

## Opening CF ports and saving configuration

```bash
bash <(curl -L -s https://gitlab.com/hugocollier/my-script/-/raw/main/x-ui/cf_ports_cent.sh)

rpm -aq iptables-services
yum install iptables-services -y
service iptables save
```

## Installing certbot

```bash
sudo dnf install epel-release
sudo dnf install certbot
certbot certonly --standalone --agree-tos --register-unsafely-without-email -d domain-yours.org
```

## Install original x-ui

```bash
bash <(curl -Ls https://raw.githubusercontent.com/mhsanaei/3x-ui/master/install.sh)
```

## Xray routing config

[https://github.com/XTLS/Xray-core/discussions/2536](https://github.com/XTLS/Xray-core/discussions/2536) - got warp all domains regexpr 

[my-routing-config](https://gitlab.com/hugocollier/my-script/-/raw/main/x-ui/warp-all-routing.json)