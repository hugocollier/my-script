#!/bin/bash

red='\033[0;31m'
green='\033[0;32m'
yellow='\033[0;33m'
plain='\033[0m'

#Add some basic function here
function LOGD() {
    echo -e "${yellow}[DEG] $* ${plain}"
}

function LOGE() {
    echo -e "${red}[ERR] $* ${plain}"
}

function LOGI() {
    echo -e "${green}[INF] $* ${plain}"
}



sudo iptables -I INPUT -s 0.0.0.0/0 -p tcp --dport 443 -j ACCEPT
sudo iptables -I INPUT -s 0.0.0.0/0 -p tcp --dport 80 -j ACCEPT
sudo iptables -I INPUT -s 0.0.0.0/0 -p tcp --dport 8080 -j ACCEPT
sudo iptables -I INPUT -s 0.0.0.0/0 -p tcp --dport 8880 -j ACCEPT
sudo iptables -I INPUT -s 0.0.0.0/0 -p tcp --dport 2052 -j ACCEPT
sudo iptables -I INPUT -s 0.0.0.0/0 -p tcp --dport 2082 -j ACCEPT
sudo iptables -I INPUT -s 0.0.0.0/0 -p tcp --dport 2086 -j ACCEPT
sudo iptables -I INPUT -s 0.0.0.0/0 -p tcp --dport 2095 -j ACCEPT
sudo iptables -I INPUT -s 0.0.0.0/0 -p tcp --dport 2053 -j ACCEPT
sudo iptables -I INPUT -s 0.0.0.0/0 -p tcp --dport 2083 -j ACCEPT
sudo iptables -I INPUT -s 0.0.0.0/0 -p tcp --dport 2087 -j ACCEPT
sudo iptables -I INPUT -s 0.0.0.0/0 -p tcp --dport 2096 -j ACCEPT
sudo iptables -I INPUT -s 0.0.0.0/0 -p tcp --dport 8443 -j ACCEPT
yum install iptables-services -y
service iptables save

LOGI "Opening Cloudflare cdn allowed HTTP, HTTPS ports successful"
